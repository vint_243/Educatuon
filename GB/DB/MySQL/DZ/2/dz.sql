/*

///
///////////////////////////////////////////////////////////
///							///
///			Geekbrains MySQL		///
///							///
///	 		DZ 2				///
///							///
///			from vint_243			///
///							///
///////////////////////////////////////////////////////////


/*
	Edit `_countries` tables 
*/



ALTER TABLE `geodata`.`_countries`
CHANGE COLUMN `country_id` `id` INT NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `title_ru` `title` VARCHAR(150) NOT NULL,
ADD PRIMARY KEY (`id`); 

CREATE UNIQUE INDEX `countries` ON  `geodata`.`_countries` (`title`); 

ALTER TABLE `geodata`.`_countries` DROP COLUMN `title_ua`, DROP COLUMN `title_be`, DROP COLUMN `title_en`, DROP COLUMN `title_es`, DROP COLUMN `title_pt`, DROP COLUMN `title_de`, DROP COLUMN `title_fr`, DROP COLUMN `title_it`, DROP COLUMN `title_pl`, DROP COLUMN `title_ja`, DROP COLUMN `title_lt`, DROP COLUMN `title_lv`, DROP COLUMN `title_cz`;


/*
	Edit `_regions` tables
*/



ALTER TABLE `geodata`.`_regions`
CHANGE COLUMN `region_id` `id` INT NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `country_id` `country_id` INT NOT NULL,
CHANGE COLUMN `title_ru` `title` VARCHAR(150) NOT NULL,
ADD PRIMARY KEY (`id`);

ALTER TABLE `geodata`.`_regions`
ADD FOREIGN KEY (country_id) REFERENCES _countries(id);

CREATE INDEX `regions` ON  `geodata`.`_regions` (`title`);

ALTER TABLE `geodata`.`_regions` DROP COLUMN `title_ua`, DROP COLUMN `title_be`, DROP COLUMN `title_en`, DROP COLUMN `title_es`, DROP COLUMN `title_pt`, DROP COLUMN `title_de`, DROP COLUMN `title_fr`, DROP COLUMN `title_it`, DROP COLUMN `title_pl`, DROP COLUMN `title_ja`, DROP COLUMN `title_lt`, DROP COLUMN `title_lv`, DROP COLUMN `title_cz`;


/*
        Edit `_cities` tables
*/


ALTER TABLE `geodata`.`_cities`
CHANGE COLUMN `city_id` `id` INT NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `country_id` `country_id` INT NOT NULL,
CHANGE COLUMN `important` `important` TINYINT(1) NOT NULL,
CHANGE COLUMN `region_id` `region_id` INT NOT NULL,
CHANGE COLUMN `title_ru` `title` VARCHAR(150) NOT NULL,
ADD PRIMARY KEY (`id`);

ALTER TABLE `geodata`.`_cities` 
ADD FOREIGN KEY (country_id) REFERENCES _countries(id);

/* 
ALTER TABLE `geodata`.`_cities`
ADD FOREIGN KEY (region_id) REFERENCES _regions(id);

*/

ALTER TABLE `geodata`.`_cities` DROP COLUMN `area_ru`, DROP COLUMN `region_ru`, DROP COLUMN `title_ua`, DROP COLUMN `area_ua`, DROP COLUMN `region_ua`, DROP COLUMN `title_be`, DROP COLUMN `area_be`, DROP COLUMN `region_be`, DROP COLUMN `title_en`, DROP COLUMN `area_en`, DROP COLUMN `region_en`, DROP COLUMN `title_es`, DROP COLUMN `area_es`, DROP COLUMN `region_es`, DROP COLUMN `title_pt`, DROP COLUMN `area_pt`, DROP COLUMN `region_pt`, DROP COLUMN `title_de`, DROP COLUMN `area_de`, DROP COLUMN `region_de`, DROP COLUMN `title_fr`, DROP COLUMN `area_fr`, DROP COLUMN `region_fr`, DROP COLUMN `title_it`, DROP COLUMN `area_it`, DROP COLUMN `region_it`, DROP COLUMN `title_pl`, DROP COLUMN `area_pl`, DROP COLUMN `region_pl`, DROP COLUMN `title_ja`, DROP COLUMN `area_ja`, DROP COLUMN `region_ja`, DROP COLUMN `title_lt`, DROP COLUMN `area_lt`, DROP COLUMN `region_lt`, DROP COLUMN `title_lv`, DROP COLUMN `area_lv`, DROP COLUMN `region_lv`, DROP COLUMN `title_cz`, DROP COLUMN `area_cz`, DROP COLUMN `region_cz`; 




