#!/usr/bin/env bash

########################## 
##			##
##    Equalent ps ax	##
##			##
##########################


tab="%-8s%-8s%-5s%-8s%-100s\n"

echo
printf "$tab" PID TTY STAT TIME COMMAND

for proc in `ls /proc/ | grep '[[:digit:]]' | sort -n` ; do
   
   if [[ -a /proc/$proc/status ]] ; then
        
	## Find pid proc
        
	pid=$proc

        ## Why tty
        
	tty=`cat /proc/$proc/stat | awk '{print $7}' ` 

        if [[ $tty == "0" ]] ; then

             tty='?'
        
	else

            # Find user terminal
        
	    uid=`grep Uid /proc/$proc/status | awk '{print $2}'`
            
	    if [[ $uid == "0" ]] ; then
            
	         user='root'

               else

                 user=`grep $uid /etc/passwd | cut -d ":" -f 1`
            fi
            
	    tty_=`ls -la /proc/$proc/fd/ | grep 'tty\|pts' | head -1 | awk '{print $11}' | cut -d "/" -f 3 `

	    if [[ -z "$tty_" ]] ; then
               
                 tty='?'
              
	       else

                 tty=$tty_
            fi
        fi

        ## Status
         
         # Main status

        s_m=`grep "State" /proc/$proc/status | awk '{ print $2 }'` 
        s_n_=`cat /proc/$proc/stat | awk '{print $19}'`
        
	if [[ $s_n_ == "0" ]] ; then
             
	      s_n=""

           else

   	    if [[ $s_n_ -ne 0 ]] ; then
	
  	          s_n="<"

	       else

	          s_n="N"
	    fi  
        fi

        # VmLck size lock memory ( see is locking memory)
        
	s_lock_=`grep VmLck /proc/$proc/status | awk '{print $2}' `

	if [[ $s_lock_ -ne 0 ]] ; then
	
	     s_lock="L"
	  
	   else
	     
	     s_lock=""
	fi

        # ID session pid user and perent 
        
	n_ss_=`grep "NSsid" /proc/$proc/status | awk '{print $2}'`	
	n_sp_=`grep "NSpid" /proc/$proc/status | awk '{print $2}' `
       
	if [[ $n_ss_ != $n_sp_ ]] ; then
            
	     s_lead=""
       	  
	   else
	    
	     s_lead="s"
        fi

        # Tracert process

        s_mlt_=`cat /proc/$proc/stat | awk '{print $20}'`
        
	if [[ $s_mlt_ -ne 1 ]] ; then
            
	     s_mlt="l"
	   
	   else	    
	   
	     s_mlt=""
        fi

        s_fg_=`cat /proc/$proc/stat | awk '{print $8}'`
        
	if [[ $s_fg_ == -1 ]] ; then 
        
	      s_fg=""
	  
	    else
           
              s_fg="+"
        fi

	## Complate status
 
        state="$s_m""$s_n""$s_lock""$s_lead""$s_mlt""$s_fg"

        ## Time 
 
        time_s=`cat /proc/$proc/stat | awk '{ print $15 }'`
	time_u=`cat /proc/$proc/stat | awk '{ print $14 }'`	
	time_sec=$(((time_s+time_u)/100))        

        time=`date -u -d @${time_sec} +"%M:%S"`

        ## Command

        com_=`tr -d '\0' </proc/$proc/cmdline`

        if [[ -z "$com_" ]] ;then
           
	     com="[`grep "Name" /proc/$proc/status | awk '{ print $2 }'`]"
         
	   else
           
             com=`echo ${com_:0:80}`
        fi
        
	### Output complate informatilon
 
        printf "$tab" $pid $tty $state $time "$com"

      else

        continue
    fi

done
                                                           
