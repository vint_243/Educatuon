
### 1/8  Управление пакетами. Дистрибьюция софта. 

##  Домашнее задание

```
Размещаем свой RPM в своем репозитории
1) создать свой RPM (можно взять свое приложение, либо собрать к примеру апач с определенными опциями)
2) создать свой репо и разместить там свой RPM
реализовать это все либо в вагранте, либо развернуть у себя через nginx и дать ссылку на репо 

* реализовать дополнительно пакет через docker

``` 
## 1) Создаем RPM

# Скачиваем спомощью  yumdownloader исходники web сервера nginx и устанавливаем их

```
yumdownloader --source nginx
cd ~/rpmbuild/SRPMS/
rpm -ihv nginx-1.12.2-2.el7.src.rpm

```

# Переходим в директорию SPECS и правим файл nginx.spec 

```
cd ~/rpmbuild/SPECS

Добавляем строки конфигурации 

    --without-http_memcached_module  \
    --without-http_gzip_module \
    --without-http_limit_conn_module \
    --without-http_access_module \
    --without-http_auth_basic_module \
    --without-http_autoindex_module  \
    --without-http_rewrite_module \

Правим немного описание пакета

```
# Для сборки нам еще понадобятся следующие пакеты

```
yum install -y gperftools-devel openssl-devel pcre-devel zlib-devel GeoIP-devel gd-devel perl-devel perl libxslt-devel perl-ExtUtils-Embed 

```

# Выполняем сборку исходников 

```
rpmbuild -bb nginx.spec

 [root@otuslinux BUILDROOT]# tree ../RPMS/
../RPMS/
|-- noarch
|   |-- nginx-all-modules-1.12.2-2.el7.centos.noarch.rpm
|   `-- nginx-filesystem-1.12.2-2.el7.centos.noarch.rpm
`-- x86_64
    |-- nginx-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-debuginfo-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64.rpm
    |-- nginx-mod-mail-1.12.2-2.el7.centos.x86_64.rpm
    `-- nginx-mod-stream-1.12.2-2.el7.centos.x86_64.rpm

```

# Пробуем установить пакеты которые собрались

```
rpm -iv ../RPMS/noarch/nginx-* ../RPMS/x86_64/nginx-*

[root@otuslinux SPECS]# yum info nginx   
Failed to set locale, defaulting to C
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.corbina.net
 * epel: mirror.23media.de
 * extras: mirror.corbina.net
 * updates: mirror.corbina.net
Installed Packages
Name        : nginx
Arch        : x86_64
Epoch       : 1
Version     : 1.12.2
Release     : 2.el7.centos
Size        : 1.5 M
Repo        : installed
Summary     : A high performance web server and reverse proxy server add module memcached gzip limit_conn access auth_basic autoindex rewrite . Build from vint_243
URL         : http://nginx.org/
License     : BSD
Description : Nginx is a web server and a reverse proxy server for HTTP, SMTP, POP3 and
            : IMAP protocols, with a strong focus on high concurrency, performance and low
            : memory usage.
```

# Посмотрим модули собранного nginx

```

[root@otuslinux RPMS]# nginx -V
nginx version: nginx/1.12.2
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-28) (GCC)
built with OpenSSL 1.0.2k-fips  26 Jan 2017
TLS SNI support enabled
configure arguments: --prefix=/usr/share/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --http-client-body-temp-path=/var/lib/nginx/tmp/client_body --http-proxy-temp-path=/var/lib/nginx/tmp/proxy --http-fastcgi-temp-path=/var/lib/nginx/tmp/fastcgi --http-uwsgi-temp-path=/var/lib/nginx/tmp/uwsgi --http-scgi-temp-path=/var/lib/nginx/tmp/scgi --pid-path=/run/nginx.pid --lock-path=/run/lock/subsys/nginx --user=nginx --group=nginx --with-file-aio --with-ipv6 --with-http_auth_request_module --with-http_ssl_module --with-http_v2_module --with-http_realip_module --with-http_addition_module --with-http_xslt_module=dynamic --with-http_image_filter_module=dynamic --with-http_geoip_module=dynamic --with-http_sub_module --with-http_dav_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module --without-http_memcached_module --without-http_gzip_module --without-http_limit_conn_module --without-http_access_module --without-http_auth_basic_module --without-http_autoindex_module --without-http_rewrite_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_degradation_module --with-http_slice_module --with-http_stub_status_module --with-http_perl_module=dynamic --with-mail=dynamic --with-mail_ssl_module --with-pcre --with-pcre-jit --with-stream=dynamic --with-stream_ssl_module --with-google_perftools_module --with-debug --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic' --with-ld-opt='-Wl,-z,relro -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -Wl,-E'

```
# Производим запуск nginx и проверяем его работоспособность

```

[root@otuslinux SPECS]# systemctl start nginx
[root@otuslinux SPECS]# systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2018-06-14 09:31:21 UTC; 4s ago
  Process: 15283 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 15281 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 15280 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 15285 (nginx)
   CGroup: /system.slice/nginx.service
           ├─15285 nginx: master process /usr/sbin/nginx
           └─15286 nginx: worker process

lynx 192.168.12.101

 Welcome to nginx on Fedora!

   This page is used to test the proper operation of the nginx HTTP server after it has been installed. If you can read this page, it means that the web server
   installed at this site is working properly.

Website Administrator

   This is the default index.html page that is distributed with nginx on Fedora. It is located in /usr/share/nginx/html.

   You should now put your content in a location of your choice and edit the root configuration directive in the nginx configuration file /etc/nginx/nginx.conf.
   [ Powered by nginx ] [ Powered by Fedora ]


```

# Веб сервер работоспособен


## 2) Создаем свой репозиторий 

# Устанавпливаем пакет createrepo и создаем репозиторий

```
yum install -y createrepo

[root@otuslinux SPECS]# createrepo /vagrant/repo/
/bin/sh: warning: setlocale: LC_ALL: cannot change locale (ru_RU.UTF-8)
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete

```

# Копируем в него наши пакеты и обновляем репозиторий

```
[root@otuslinux RPMS]# cp -vR ./* /vagrant/repo/
'./noarch' -> '/vagrant/noarch'
'./noarch/nginx-all-modules-1.12.2-2.el7.centos.noarch.rpm' -> '/vagrant/repo/noarch/nginx-all-modules-1.12.2-2.el7.centos.noarch.rpm'
'./noarch/nginx-filesystem-1.12.2-2.el7.centos.noarch.rpm' -> '/vagrant/repo/noarch/nginx-filesystem-1.12.2-2.el7.centos.noarch.rpm'
'./x86_64' -> '/vagrant/repo/x86_64'
'./x86_64/nginx-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-mail-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-mail-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-mod-stream-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-mod-stream-1.12.2-2.el7.centos.x86_64.rpm'
'./x86_64/nginx-debuginfo-1.12.2-2.el7.centos.x86_64.rpm' -> '/vagrant/repo/x86_64/nginx-debuginfo-1.12.2-2.el7.centos.x86_64.rpm'

[root@otuslinux RPMS]# ls -la /vagrant/repo/
total 8
drwxr-xr-x. 5 root root   50 Jun 14 09:38 .
drwxr-xr-x. 3 root root  111 Jun 14 09:36 ..
drwxr-xr-x. 2 root root  117 Jun 14 09:38 noarch
drwxr-xr-x. 2 root root 4096 Jun 14 09:37 repodata
drwxr-xr-x. 2 root root 4096 Jun 14 09:38 x86_64

[root@otuslinux RPMS]# createrepo --update /vagrant/repo/
/bin/sh: warning: setlocale: LC_ALL: cannot change locale (ru_RU.UTF-8)
Spawning worker 0 with 10 pkgs
Workers Finished
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete

```

# Создаем файл своего репозитория

```
[root@otuslinux RPMS]# cat /etc/yum.repos.d/vint_243.repo 
[vint_243]
name=vint_243
baseurl=http://repo.raz-net.com/centos/
gpgcheck=0
enabled=1

```

# Устанавливаем пакеты из нового репозитория

```

[root@otuslinux RPMS]# yum provides nginx

1:nginx-1.12.2-2.el7.centos.x86_64 : A high performance web server and reverse proxy server add module memcached gzip limit_conn access auth_basic autoindex rewrite .
                                   : Build from vint_243
Repo        : vint_243

[root@otuslinux RPMS]# yum install nginx

Dependencies Resolved

========================================================================================================================================================================
 Package                                            Arch                          Version                                         Repository                       Size
========================================================================================================================================================================
Installing:
 nginx                                              x86_64                        1:1.12.2-2.el7.centos                           vint_243                        513 k
Installing for dependencies:
 nginx-all-modules                                  noarch                        1:1.12.2-2.el7.centos                           vint_243                         15 k
 nginx-filesystem                                   noarch                        1:1.12.2-2.el7.centos                           vint_243                         16 k
 nginx-mod-http-geoip                               x86_64                        1:1.12.2-2.el7.centos                           vint_243                         22 k
 nginx-mod-http-image-filter                        x86_64                        1:1.12.2-2.el7.centos                           vint_243                         25 k
 nginx-mod-http-perl                                x86_64                        1:1.12.2-2.el7.centos                           vint_243                         34 k
 nginx-mod-http-xslt-filter                         x86_64                        1:1.12.2-2.el7.centos                           vint_243                         24 k
 nginx-mod-mail                                     x86_64                        1:1.12.2-2.el7.centos                           vint_243                         52 k
 nginx-mod-stream                                   x86_64                        1:1.12.2-2.el7.centos                           vint_243                         75 k

Transaction Summary
========================================================================================================================================================================
Install  1 Package (+8 Dependent packages)

Total download size: 777 k
Installed size: 1.8 M
Is this ok [y/d/N]: y
Downloading packages:
(1/9): nginx-all-modules-1.12.2-2.el7.centos.noarch.rpm                                                                                          |  15 kB  00:00:00
(2/9): nginx-filesystem-1.12.2-2.el7.centos.noarch.rpm                                                                                           |  16 kB  00:00:00
(3/9): nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64.rpm                                                                                       |  22 kB  00:00:00
(4/9): nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64.rpm                                                                                |  25 kB  00:00:00
(5/9): nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64.rpm                                                                                        |  34 kB  00:00:00
(6/9): nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64.rpm                                                                                 |  24 kB  00:00:00
(7/9): nginx-mod-mail-1.12.2-2.el7.centos.x86_64.rpm                                                                                             |  52 kB  00:00:00
(8/9): nginx-mod-stream-1.12.2-2.el7.centos.x86_64.rpm                                                                                           |  75 kB  00:00:00
(9/9): nginx-1.12.2-2.el7.centos.x86_64.rpm                                                                                                      | 513 kB  00:00:01
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                   493 kB/s | 777 kB  00:00:01
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : 1:nginx-filesystem-1.12.2-2.el7.centos.noarch                                                                                                        1/9
  Installing : 1:nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64                                                                                                    2/9
  Installing : 1:nginx-mod-stream-1.12.2-2.el7.centos.x86_64                                                                                                        3/9
  Installing : 1:nginx-mod-mail-1.12.2-2.el7.centos.x86_64                                                                                                          4/9
  Installing : 1:nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64                                                                                              5/9
  Installing : 1:nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64                                                                                                     6/9
  Installing : 1:nginx-1.12.2-2.el7.centos.x86_64                                                                                                                   7/9
  Installing : 1:nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64                                                                                             8/9
  Installing : 1:nginx-all-modules-1.12.2-2.el7.centos.noarch                                                                                                       9/9
  Verifying  : 1:nginx-mod-http-image-filter-1.12.2-2.el7.centos.x86_64                                                                                             1/9
  Verifying  : 1:nginx-mod-http-geoip-1.12.2-2.el7.centos.x86_64                                                                                                    2/9
  Verifying  : 1:nginx-mod-stream-1.12.2-2.el7.centos.x86_64                                                                                                        3/9
  Verifying  : 1:nginx-filesystem-1.12.2-2.el7.centos.noarch                                                                                                        4/9
  Verifying  : 1:nginx-mod-mail-1.12.2-2.el7.centos.x86_64                                                                                                          5/9
  Verifying  : 1:nginx-1.12.2-2.el7.centos.x86_64                                                                                                                   6/9
  Verifying  : 1:nginx-mod-http-xslt-filter-1.12.2-2.el7.centos.x86_64                                                                                              7/9
  Verifying  : 1:nginx-all-modules-1.12.2-2.el7.centos.noarch                                                                                                       8/9
  Verifying  : 1:nginx-mod-http-perl-1.12.2-2.el7.centos.x86_64                                                                                                     9/9

Installed:
  nginx.x86_64 1:1.12.2-2.el7.centos

Dependency Installed:
  nginx-all-modules.noarch 1:1.12.2-2.el7.centos            nginx-filesystem.noarch 1:1.12.2-2.el7.centos     nginx-mod-http-geoip.x86_64 1:1.12.2-2.el7.centos
  nginx-mod-http-image-filter.x86_64 1:1.12.2-2.el7.centos  nginx-mod-http-perl.x86_64 1:1.12.2-2.el7.centos  nginx-mod-http-xslt-filter.x86_64 1:1.12.2-2.el7.centos
  nginx-mod-mail.x86_64 1:1.12.2-2.el7.centos               nginx-mod-stream.x86_64 1:1.12.2-2.el7.centos

Complete!

```
# Запускаем nginx и проверяем его работоспособность

```
[root@otuslinux RPMS]# systemctl start nginx
[root@otuslinux RPMS]# systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2018-06-14 09:58:07 UTC; 4s ago
  Process: 3930 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 3927 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 3926 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 3932 (nginx)
   CGroup: /system.slice/nginx.service
           ├─3932 nginx: master process /usr/sbin/nginx
           └─3933 nginx: worker process

lynx 192.168.12.101

 Welcome to nginx on Fedora!

   This page is used to test the proper operation of the nginx HTTP server after it has been installed. If you can read this page, it means that the web server
   installed at this site is working properly.

Website Administrator

   This is the default index.html page that is distributed with nginx on Fedora. It is located in /usr/share/nginx/html.

   You should now put your content in a location of your choice and edit the root configuration directive in the nginx configuration file /etc/nginx/nginx.conf.
   [ Powered by nginx ] [ Powered by Fedora ]


```


